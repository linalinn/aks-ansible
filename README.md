### How to execute the ansible playbooks
Using the aks mini template:

```bash run.sh ansible-playbook -c local \ --vault-password-file="password.secret" --extra-vars="@vaults/secrets.valut" --extra-vars="@templates/aks-mini-vars.yml" aks-deploy.yml```


### Vars contained in vault
 - azure_client_id
 - azure_client_secret
 - azure_tenant
 - azure_subscription_id

 ### Get client id, secret and tenant via azure cli by creating an serviceprincipal
 `az ad sp create-for-rbac --name ansible`